const { personalData, preferencesColor } = require('../Controllers')

function Servicio() {
    const personal = personalData()
    const color = preferencesColor()

    console.log(personal, color)
    return {personal, color}
}

module.exports = { Servicio }