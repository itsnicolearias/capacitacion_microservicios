function personalData() {
    return { name: 'Nicole', age: 22}
}

function preferencesColor() {
    return ['white', 'orange']
}

module.exports = { personalData, preferencesColor }