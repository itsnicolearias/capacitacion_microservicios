const { Servicio } = require('../Services')

function Adaptador(info, color){
    const { color, personal } = Servicio()
    const usuario = { color, personal }
    return usuario;
}

module.exports = { Adaptador }